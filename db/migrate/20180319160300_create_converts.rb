class CreateConverts < ActiveRecord::Migration[5.0]
  def change
    create_table :converts do |t|
      t.string :name
      t.decimal :price

      t.timestamps
    end
  end
end
