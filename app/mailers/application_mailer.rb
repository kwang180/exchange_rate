class ApplicationMailer < ActionMailer::Base
  default from: 'kwang180@gmail.com'
  layout 'mailer'

  def sample_email(tomail,optionsave,targets,rates)
    @optionsave = optionsave
    @targets = targets
    @rates = rates
    mail(to: tomail, subject: 'Sample Email')
  end
  def sample_email1(tomail,option,option1,rate)
    @option = option
    @option1 = option1
    @rate = rate
    mail(to: tomail, subject: 'Sample Email')
  end
  def sample_email2(tomail,option,option1,rate,amount)
    @option = option
    @amount = amount
    @option1 = option1
    @rate = rate
    mail(to: tomail, subject: 'Sample Email')
  end

end
