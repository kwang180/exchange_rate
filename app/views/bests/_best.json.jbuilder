json.extract! best, :id, :name, :price, :created_at, :updated_at
json.url best_url(best, format: :json)
